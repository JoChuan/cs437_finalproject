from picamera import PiCamera
from time import sleep
from datetime import datetime as dt

import json
import logging
import subprocess
import time
import cv2
import numpy as np
import picar_4wd as fc
import requests

speed = 10
status = 400
url = 's'

def main():
    camera = PiCamera()
    while True:
        Track_line()
        camera.start_preview()
        current_date = dt.now()
        timestamp = int(dt.timestamp(dt.now()))
        camera.capture(str(current_date) +'.jpg')
        image = str(current_date)+'.jpg'
 
        process = subprocess.run(['alpr','-c eu','-p cz','-n 3','-j',image],
            stdout=subprocess.PIPE,text=True)
        
        diccionario = process.stdout
        diccionario = json.loads(diccionario)
        if diccionario['results']==[]:
            print('No plates detected')

        # the car moving all the time until detect a plate
        # when detect, fc.stop()
        elif diccionario['results']!=[]:
            fc.stop()
            myobj = {'plate_number': diccionario['results'][0]['plate'], 
                        'timestamp': timestamp}
            x = requests.post(url, json = myobj)
            print(x.text)
            print(diccionario['results'][0]['plate'])
            print(diccionario['results'][0]['confidence'])
            print(current_date)
            camera.stop_preview()

def Track_line():
    gs_list = fc.get_grayscale_list()
    if fc.get_line_status(status, gs_list) == 0:
        fc.forward(Track_line_speed) 
    elif fc.get_line_status(status, gs_list) == -1:
        fc.turn_left(Track_line_speed)
    elif fc.get_line_status(status, gs_list) == 1:
        fc.turn_right(Track_line_speed) 

main()

