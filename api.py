import json
from flask import Flask, request, jsonify, 
  send_from_directory, render_template 
from flask_cors import CORS
import mysql.connector

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="12345678",
  database="iot"
)
mycursor = mydb.cursor()

app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": "*"}})

@app.route('/', methods=['GET'])
def index():
  # return send_from_directory(app.static_folder, "index.html")
  return jsonify(success=True)

@app.route('/park', methods=['POST'])
def park():
  plateList = []
  body = json.loads(request.data)

  if body['plate_number'] is None:
    return jsonify(error="parameter `plate_number` not found")
  if body['timestamp'] is None:
    return jsonify(error="parameter `timestamp` not found")

  print(body['plate_number'], body['timestamp'])

  query = """INSERT INTO Parking (Plate) VALUES (%s)"""
  plateList.append(body['plate_number'])
  mycursor.execute(query, plateList)
  mydb.commit()
  return jsonify(success=True)

@app.route('/map', methods=['GET'])
def map():
  # resp = {'A1':'CD8051', 'A4':'UR9487', 'A6':'DM3388'}
  mycursor.execute("SELECT Loc, Plate FROM Parking WHERE Timestamp > (NOW() - INTERVAL 30 MINUTE)")
  plate = mycursor.fetchall()
  resp={}
  for it in plate:
    if it[0] is None:
      continue
    resp[it[0]] = it[1]
  return jsonify(resp)

@app.route('/bill', methods=['GET'])
def bill():
  # get the bill from MySQL with the plate number that is not paid yet
  # resp = {'001':'100542', '002':'286738', '003':'308276'}
  resp = {}
  plate = request.args.get('plate')
  plateList = []
  plateList.append(plate)

  # query db
  query = """SELECT ID, Timestamp FROM Parking WHERE Plate = %s AND Paid = False"""
  mycursor.execute(query, plateList)
  time = mycursor.fetchall()

  # construct resp
  for it in time:
      resp[it[0]] = it[1]
  print(plateList[0])

  return jsonify(resp)

@app.route('/pay', methods=['POST'])
def pay():
  # set the entries that match the id to paid
  body = json.loads(request.data)
  for it in body:
    query = """UPDATE Parking SET Paid = True WHERE ID = %s"""
    itList = []
    itList.append(it)
    print(itList)
    mycursor.execute(query, itList)

  mydb.commit()
  return jsonify(success=True)

app.run(debug=True, host='192.168.0.157', port=8080)
