document.getElementById("defaultOpen").click();
var idList = [];
var num = 0;
var lotNum = 8;

fetch('http://192.168.0.157:8080/map')
  .then(response => {
    //handle response            
    return response.json();
  })
  .then(data => {
    //handle data
    console.log(data);
    console.log(data["A1"]);
    for(var k in data){
      num++;
      console.log(k + " -> " + data[k]);
      document.getElementById(k).innerHTML = data[k];
      document.getElementById(k).style.backgroundColor = "red";
    }
  document.getElementById("occupied").innerHTML = num;
  document.getElementById("available").innerHTML = lotNum - num;
  });

function openTab(evt, tabName) {
  // Declare all variables
  var i, tabcontent, tablinks;

  // Get all elements with class="tabcontent" and hide them
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }

  // Get all elements with class="tablinks" and remove the class "active"
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }

  // Show the current tab, and add an "active" class to the button that opened the tab
  document.getElementById(tabName).style.display = "block";
  evt.currentTarget.className += " active";
}

function getBill() {
  idList = [];
  var counter = 0;
  var table = document.getElementById("paymentTable");
  var plate = document.getElementById("plate").value;
  table.style.display = "none";
  table.innerHTML = "";

  fetch('http://192.168.0.157:8080/bill?plate=' + plate)
  .then(function(response){
    //handle response            
    return response.json();
  })
  .then(function(data) {
    document.getElementById("nodataAfterPay").innerHTML = "";
    if(Object.keys(data).length == 0){
      document.getElementById("nodata").innerHTML = "No Data";
    }else{
      document.getElementById("nodata").innerHTML = "";
      table.style.display = "table";
      var row = table.insertRow(-1);
      var c1 = row.insertCell(0);
      var c2 = row.insertCell(1);

      row.style.backgroundColor = "#00539F";
      row.style.color = "white";
      c1.innerHTML = "Timestamp";
      c2.innerHTML = "Price";

      for (var i in data) {
        idList.push(i);
        counter++;
        var row = table.insertRow(-1);
        var c1 = row.insertCell(0);
        var c2 = row.insertCell(1);

        row.style.backgroundColor = "#dddddd";
        c1.innerHTML = data[i];
        c2.innerHTML = "$15";
      }
    }
    document.getElementById("total").innerHTML = counter * 15;
  });
}


function pay(){
  // js post request
  fetch("http://192.168.0.157:8080/pay", {
    method: "POST",
    headers: {'Content-Type': 'application/json'}, 
    body: JSON.stringify(idList)
  }).then(res => {
    console.log("Request complete! response:", res);
  });
  document.getElementById("nodataAfterPay").innerHTML = "No Data";
  var table = document.getElementById("paymentTable");
  table.style.display = "none";
  table.innerHTML = "";
  document.getElementById("total").innerHTML = 0;
}

